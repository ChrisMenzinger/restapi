import * as React from 'react';
import {useState} from "react";

import Button from '@mui/material/Button';
import {TextField} from "@mui/material";
import axios from "axios";

export function CreateComment(addComment) {
    const [content, setContent] = useState("");
    const buttonClick = () => {

        setContent("")
        axios.post(`/api/comments`, {content: content})
    }

    return (
        <div style={styles.screen}>
            <TextField sx={styles.textField} value={content} onChange={(event) => {
                setContent(event.target.value);
            }}></TextField>
            <h4 style={styles.counter}>{`${content.length} / 220`}</h4>
            <Button style={styles.button} onClick={buttonClick}>Add Comment</Button>
        </div>
    )
}

const styles = {
    screen: {
        display: "flex",
        justifyContent: "center",
        width: "90vw",
        height: 200,
        backgroundColor: "#3D3D40",
        borderRadius: 10,
        position: "relative",
        margin: 10,
    },
    textField: {
        margin: 2,
        height: 55,
        borderRadius: 2,
        backgroundColor: "white",
        width: "89vw",

    },
    counter: {
        position: "absolute",
        bottom: 127,
        right: 19,
        margin: "0",
        padding: "4px",
        fontSize: "14px",
    },
    button: {
        position: "absolute",
        color: "black",
        bottom: 62,
        borderRadius: 7,
        right: 17,
        backgroundColor: "white",
        height: 50,
        width: 150,
        "&:hover": {
            backgroundColor: "grey",
        }
    }
}
