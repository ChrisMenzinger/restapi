import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import {IconButton} from '@mui/material';
import Typography from '@mui/material/Typography';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from "axios";

export default function Comment(comment) {

    const deleteComment = () => {
        axios.delete(`/api/comments/${comment.comment.id}`)
    }

    return (
        <Card sx={styles.card}>
            <CardContent>
                <Typography sx={styles.header}>
                    <IconButton>
                        <DeleteIcon onClick={deleteComment}/>
                    </IconButton>

                </Typography>
                <Typography variant="body2">
                    {comment.comment.content}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">Edit</Button>
            </CardActions>
        </Card>
    );
}


const styles = {
    card: {
        width: 400,
        margin: 5,
    },
    header: {
        display: 'flex',
        flexDirection: "row-reverse",
    }
}

