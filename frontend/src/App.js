import React, {useEffect, useState} from "react"
import Comment from "./components/Comment"
import {CreateComment} from "./components/CreateComment";
import axios from "axios";

export function App() {

    const [comments, setComments] = useState([{}])

    useEffect(() => {
        axios.get("/api/getAllComments").then((res) => {
            setComments(res.data)
        })
    },)

    const addComment = (newComment) => {
        setComments([...comments, newComment]);
    };

    return (
        <div>
            <div>
                {comments?.map((comment) => {
                    return <Comment key={comment.id} comment={comment}/>
                })}
            </div>
            <div style={styles.createCommentContainer}>
                <CreateComment addComment={addComment}/>
            </div>
        </div>
    )
}

const styles = {
    createCommentContainer: {
        position: "sticky",
        bottom: 0,
    }
}