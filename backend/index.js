const express = require('express');
const {randomBytes} = require('crypto')
const bodyParser = require("body-parser");


const commentsWithId = [];

const app = express();
app.use(bodyParser.json())


app.get('/api/getAllComments', (req, res) => {
    res.send(commentsWithId || []);
})

app.post("/api/comments", (req, res) => {
    const commentId = randomBytes(4).toString("hex")
    const {content} = req.body;

    commentsWithId.push({id: commentId, content})

    res.status(201).send("Add Comment with ID: " + commentId);
})

app.delete("/api/comments/:id", (req, res) => {
    const {id} = req.params;
    const commentToDeleteIndex = commentsWithId.findIndex(comment => comment.id === id);
    if (commentToDeleteIndex !== -1) {
        commentsWithId.splice(commentToDeleteIndex, 1);
        res.status(201).send("Delete Comment with ID: " + id);
    } else {
        res.status(404).send("Comment not found");
    }
})

app.listen(4000, () => {
    console.log("Server started on port 4000")
})